package com.tordre.LD27;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;

public class CreditScreen implements Screen {
	final LD27 game;
	float time = 0.0f;

	private OrthographicCamera camera;
	
	private Texture texCredits;
	
	public CreditScreen(final LD27 game) {
        this.game = game;        

		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false,w,h);

		texCredits = new Texture(Gdx.files.internal("data/Credits.png"));
    }

	@Override
	public void render(float delta) {
		time += delta;
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		game.batch.setProjectionMatrix(camera.combined);
		
		game.batch.begin();
		game.batch.draw(texCredits, 0, 480-1024);
		
		if((Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) && time > 0.5) || (time>2)){
			game.setScreen(new TitleScreen(game));
            dispose();
		}
		
		game.batch.end();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		texCredits.dispose();
	}

}
