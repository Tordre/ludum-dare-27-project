package com.tordre.LD27;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class GameOverScreen implements Screen {
	final LD27 game;
	private OrthographicCamera camera;
	
	float time;
	
	public GameOverScreen(final LD27 game) {
		this.game = game;
		
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		camera = new OrthographicCamera();
		camera.setToOrtho(true,w,h);
		
		
	}

	@Override
	public void render(float delta) {
		time += delta;
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		game.batch.setProjectionMatrix(camera.combined);
		
		game.batch.begin();

		game.batch.enableBlending();
		game.font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		game.font.draw(game.batch, "GAME OVER", 220.0f, 200.0f);
		game.font.draw(game.batch, " YOU SUCK", 220.0f, 232.0f);
		
		game.font.draw(game.batch, "PRESS ANY KEY TO TRY HARDER", 45.0f, 400.0f);
		game.font.draw(game.batch, "           n00b            ", 45.0f, 432.0f);
		
		if(Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) && (time > 0.5)){
			game.sfxSelect.play(game.sfxVolume);
			game.setScreen(new HighScoreScreen(game));
		}
		game.batch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
