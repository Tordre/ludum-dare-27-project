package com.tordre.LD27;

import java.text.DecimalFormat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;

public class GameScreen implements Screen {
	final LD27 game;
	private OrthographicCamera camera;
	
	//Variables for the Ship
	private Texture texShip;
	private TextureRegion shipForward;
	private TextureRegion shipLeanLeft;
	private TextureRegion shipLeanRight;
	Vector3 shipPosition;
	Vector3 shipVelocity;
	
	//Variables for the stars
	private Texture texStar;
	private Vector3[] stars;
	static int STAR_COUNT = 250;
	
	//Variables for the Power-Ups
	private Texture texPowerUp;
	private Vector3[] powerUps;
	static int POWER_UP_COUNT = 2;
	
	//Variables for the Power-Ups
	private Texture texHazzard;
	private Vector3[] hazzards;
	int activeHazzard = 10;
	static int HAZZARD_COUNT = 100; 
	
	//Variables for count down at the end
	private boolean[] countdown;
	
	
	double dialtionCoefient = 1;
	
	double distance = 0d;
	float time;
		
	//private float fps;
	
	public GameScreen(final LD27 game) {
		this.game = game;
		
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		camera = new OrthographicCamera();
		camera.setToOrtho(true,w,h);
		
		texShip = new Texture(Gdx.files.internal("data/game_sprites/ship.png"));
		shipForward = 	new TextureRegion(texShip, 64, 64, 64,-64);
		shipLeanLeft = 	new TextureRegion(texShip,  0, 64, 64,-64);
		shipLeanRight = new TextureRegion(texShip,128, 64, 64,-64);
		shipPosition = new Vector3(	(Gdx.graphics.getWidth() / 2)-32,
				(Gdx.graphics.getHeight() - 64 - 64 ),
				0);
		shipVelocity = new Vector3 ( 350,750,0);

		
		texStar = new Texture(Gdx.files.internal("data/game_sprites/star.png"));
		stars = new Vector3[STAR_COUNT];
		for(int i = 0; i < STAR_COUNT; i++){
			stars[i] = new Vector3(	(float)(Math.random() * Gdx.graphics.getWidth()), 
									(float)(-1000 + Math.random() * Gdx.graphics.getHeight() + 1000),
									(float)(1 + Math.random() * 4));
			
		}
		
		texPowerUp = new Texture(Gdx.files.internal("data/game_sprites/powerup.png"));
		powerUps = new Vector3[POWER_UP_COUNT];
		for(int i = 0; i < POWER_UP_COUNT; i++){
			powerUps[i] = new Vector3(	(float)(Math.random() * Gdx.graphics.getWidth()), 
										(float)(-1000 + Math.random() * 1000),
										(float)(1 + Math.random() * 2));
		}
		
		texHazzard = new Texture(Gdx.files.internal("data/game_sprites/hazzard.png"));
		hazzards = new Vector3[HAZZARD_COUNT];
		for(int i = 0; i < HAZZARD_COUNT; i++){
			hazzards[i] = new Vector3(	(float)(Math.random() * Gdx.graphics.getWidth()), 
									(float)(-1000 + Math.random() * 1000),
									(float)(2 + Math.random() * 2));
		}
		
		countdown = new boolean[5];
		time = 10;
	}
		
	@Override
	public void render(float delta) {
		float dilatedTime = (float) (delta * this.dialtionCoefient);
		
		//Clears the screen
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		//Resets the projection matrix
		game.batch.setProjectionMatrix(camera.combined);
		game.batch.begin();
		
		
		//Draw and Update stars
		for(Vector3 star : stars){
			game.batch.draw(texStar, star.x, star.y);
			star.y += dilatedTime * shipVelocity.y / star.z;
			if	(star.y > 480){
				star.x = (float)(Math.random() * Gdx.graphics.getWidth()); 
				star.y = (float)(Math.random() * -1000);
				star.z = (float)(1 + Math.random() * 4);
			}
		}
		
		//Draw and Update Power-Ups		
		for(Vector3 powerUp : powerUps){
			game.batch.draw(texPowerUp, powerUp.x, powerUp.y);
			powerUp.y += dilatedTime * shipVelocity.y / powerUp.z;
			
			//Collision Detection inside
			if ((powerUp.y > shipPosition.y) && (powerUp.y < shipPosition.y + shipForward.getRegionHeight())){
				if (((powerUp.x  > shipPosition.x - texPowerUp.getWidth()) && (powerUp.x < shipPosition.x + shipForward.getRegionWidth()))){
					powerUp.y = 490;
					game.sfxPowerUp.play(game.sfxVolume);
					time += 10;
				}
			}
			if	(powerUp.y > 480){
				powerUp.x = (float)(Math.random() * Gdx.graphics.getWidth()); 
				powerUp.y = (float)(Math.random() * -1000);
				powerUp.z = (float)(1 + Math.random() * 2);
			}			
		}	
		
		
		//Draw and update Asteroids
		activeHazzard = 10 + (int)(distance / 2000); 
		activeHazzard = (activeHazzard < HAZZARD_COUNT) ? activeHazzard : HAZZARD_COUNT;
		for(int i = 0; i < activeHazzard; i++){
			Vector3 hazzard = hazzards[i];
			game.batch.draw(texHazzard, hazzard.x, hazzard.y);
			hazzard.y += dilatedTime * shipVelocity.y / hazzard.z;
			
			if ((hazzard.y > shipPosition.y - texHazzard.getHeight() + 16) && (hazzard.y < shipPosition.y + shipForward.getRegionHeight() - 16)){
				if (((hazzard.x  > shipPosition.x - texHazzard.getWidth() + 16) && (hazzard.x < shipPosition.x + shipForward.getRegionWidth() - 16))){
					//play bad noise
					game.sfxHazzard.play(game.sfxVolume);
					gameOver();
				}
			}
			if	(hazzard.y > 480){
				hazzard.x = (float)(Math.random() * Gdx.graphics.getWidth()); 
				hazzard.y = (float)(Math.random() * -1000);
				hazzard.z = (float)(2 + Math.random() * 2);
			}
		}

		
		//draw ship
		if(Gdx.input.isKeyPressed(Input.Keys.UP)){
			shipPosition.y -= (shipPosition.y > 0 ) ? (delta * shipVelocity.x) : 0;
		}
		else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)){
			shipPosition.y += (shipPosition.y < 480 - 32 ) ? (delta * shipVelocity.x) : 0;
		}
		
		//draw ship
		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
			game.batch.draw(shipLeanLeft, shipPosition.x, shipPosition.y);
			shipPosition.x -= (shipPosition.x > - 32 ) ? (delta * shipVelocity.x) : 0;
		}
		else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
			game.batch.draw(shipLeanRight, shipPosition.x, shipPosition.y);
			shipPosition.x += (shipPosition.x < 640 - 32 ) ? (delta * shipVelocity.x) : 0;
		}
		else{
			game.batch.draw(shipForward, shipPosition.x, shipPosition.y);
		}
		
		//Update distance traveled
		distance += dilatedTime * shipVelocity.y;
		
		//Update time left
		time -= dilatedTime;
				
	
		//if you get more time this will reset the boolean values for the beeps
		if(time > 5){
			for(int i = 0; i < 5; i++){
				this.countdown[i] = true;
			}
		} else {
			//Beeps on the seconds, sets boolean values so it only beeps once per second.
			//There probably is a nicer way to do this
			for(int i = 5; i > 0; i--){
				if((time < i) && this.countdown[i-1]){
					this.countdown[i - 1] = false;
					game.sfxCountDown.play(game.sfxVolume);
				}			
			}
		}
		
		//Second game over state, times up
		if(time <= 0){
			game.sfxTimeUp.play(game.sfxVolume);
			this.gameOver();
		}
		
		//Formats and displays time left
		DecimalFormat formatTime = new DecimalFormat("#.0000");
		game.font.draw(game.batch, "Time: "+ formatTime.format(time), 0, 10);
		
		//Formats and displays distance traveled
		DecimalFormat formatDistance = new DecimalFormat("#.00");
		game.font.draw(game.batch, "Distance: "+ formatDistance.format(distance) + " meters", 0, 42);
		
		game.batch.end();
	}
	public void gameOver(){
		game.highScore.putFloat("11", (float)this.distance);
		game.setScreen(new GameOverScreen(game));
		dispose();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		this.texHazzard.dispose();
		this.texPowerUp.dispose();
		this.texShip.dispose();
		this.texStar.dispose();		
	}

}
