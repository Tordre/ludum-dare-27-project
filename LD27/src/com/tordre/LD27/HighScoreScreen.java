package com.tordre.LD27;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class HighScoreScreen implements Screen {
	final LD27 game;
	private OrthographicCamera camera;
	
	float time;
	
	int highLightScore = 11;
	
	public HighScoreScreen(final LD27 game) {
		this.game = game;
		
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		camera = new OrthographicCamera();
		camera.setToOrtho(true,w,h);
		float newScore = game.highScore.getFloat("11");
		
		for(int i = 10; i >0; i--){
			if(newScore > game.highScore.getFloat(Integer.toString(i))){
				game.highScore.putFloat( 	Integer.toString(i+1), 
											game.highScore.getFloat(Integer.toString(i)));
				game.highScore.putFloat( Integer.toString(i), newScore);
				this.highLightScore = i;
			}
			else{
				break;
			}
		}
		game.highScore.flush();
	}

	@Override
	public void render(float delta) {
		time += delta;
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		game.batch.setProjectionMatrix(camera.combined);
		
		game.batch.begin();

		game.batch.enableBlending();
		game.font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		game.font.draw(game.batch, "HIGH SCORE", 220.0f, 32.0f);
		for(int i = 1; i < 10; i++){
			if(highLightScore == i){
				game.font.setColor(0.0f, 1.0f, 0.0f, 1.0f);
				game.font.draw(game.batch, "This n00b: " + game.highScore.getFloat(Integer.toString(i)) + "m", 32.0f, 32.0f + 32.0f*i);
			}
			else{
				game.font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
				game.font.draw(game.batch, "Some n00b: " + game.highScore.getFloat(Integer.toString(i)) + "m", 32.0f, 32.0f + 32.0f*i);
			}
			
		}
		
		game.font.draw(game.batch, "PRESS ANY KEY TO TRY HARDER", 45.0f, 400.0f);
		game.font.draw(game.batch, "           n00b            ", 45.0f, 432.0f);
		
		if(Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) && (time > 0.5)){
			game.sfxSelect.play(game.sfxVolume);
			game.setScreen(new TitleScreen(game));
		}
		game.batch.end();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
