package com.tordre.LD27;

import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class LD27 extends Game {
	//store font here because everything will use it
	SpriteBatch batch;
	BitmapFont font;
	Preferences highScore;
	Audio audio;
	
	float musicVolume = 1.0f;
	Music background;
	
	float sfxVolume = 0.5f;
	Sound sfxPowerUp;
	Sound sfxHazzard;
	Sound sfxCountDown;
	Sound sfxTimeUp;
	Sound sfxSelect;
	
	@Override
	public void create() {		
		batch = new SpriteBatch();
		font = new BitmapFont(	Gdx.files.internal("data/font/lcd solid.fnt"),
		         				Gdx.files.internal("data/font/lcd solid_0.png"), true);
		   
        highScore = Gdx.app.getPreferences("com.tordre.ld27.HighScore");
        for(int i = 1; i <= 10; i++){
            if(!highScore.contains(Integer.toString(i)))
            	highScore.putFloat(Integer.toString(i), 1000.0f);
        }
        audio = Gdx.audio;
        background = audio.newMusic(Gdx.files.internal("data/music/BG music.ogg"));
        background.setLooping(true);
        background.setVolume(musicVolume);
        background.play();
        
        sfxPowerUp = audio.newSound(Gdx.files.internal("data/sfx/Pickup_Coin15.wav"));
        sfxHazzard = audio.newSound(Gdx.files.internal("data/sfx/Explosion41.wav"));
        sfxCountDown = audio.newSound(Gdx.files.internal("data/sfx/Blip_Select71.wav"));
        sfxTimeUp = audio.newSound(Gdx.files.internal("data/sfx/Randomize7.wav"));
        sfxSelect = audio.newSound(Gdx.files.internal("data/sfx/Blip_Select75.wav"));
        
        this.setScreen(new CreditScreen(this));
	}

    public void render() {
        super.render(); //important!
	}
	
	public void dispose() {
	        batch.dispose();
	        font.dispose();
	        
	        background.dispose();
	        
	        sfxPowerUp.dispose();
	        sfxHazzard.dispose();
	        sfxCountDown.dispose();
	        sfxTimeUp.dispose();
	        sfxSelect.dispose();
	        
	}
}
