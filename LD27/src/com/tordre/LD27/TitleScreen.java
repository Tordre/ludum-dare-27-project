package com.tordre.LD27;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class TitleScreen implements Screen{
	final LD27 game;
	float time = 0.0f;

	private OrthographicCamera camera;
	
	
	public TitleScreen(final LD27 game) {
        this.game = game;        

		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		camera = new OrthographicCamera();
		camera.setToOrtho(true,w,h);
		
    }
	
	@Override
	public void render(float delta) {
		time += delta;
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		game.batch.setProjectionMatrix(camera.combined);
		
		game.batch.begin();

		game.batch.enableBlending();
		game.font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		game.font.draw(game.batch, "10 Seconds of Gas", 25.0f, 32.0f);
		game.font.draw(game.batch, "AKA Gone in 10 Seconds", 25.0f, 64.0f);
		game.font.draw(game.batch, "AKA 10 Fast 10 Furious", 25.0f, 96.0f);
		game.font.draw(game.batch, "AKA Driv10r", 25.0f, 128.0f);
		game.font.draw(game.batch, "AKA id10t error has occured", 25.0f, 160.0f);
		game.font.draw(game.batch, "AKA not the 10s you are looking for", 25.0f, 192.0f);
		game.font.draw(game.batch, "AKA I swear it might be a game", 25.0f, 224.0f);
		game.font.draw(game.batch, "AKA It might even be fun", 25.0f, 256.0f);
		
		game.font.draw(game.batch, "PRESS SPACE", 200.0f, 400.0f);
		
		if(Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) && time > 0.5){
			game.setScreen(new GameScreen(game));
			game.sfxSelect.play(game.sfxVolume);
            dispose();
		}
		game.batch.end();
	}
	
	@Override
	public void resize(int width, int height) {
	}
	
	@Override
	public void show() {
	       
	}
	
	@Override
	public void hide() {
	}
	
	@Override
	public void pause() {
	}
	
	@Override
	public void resume() {
	}
	
	@Override
	public void dispose() {
	       
	}
	
}
